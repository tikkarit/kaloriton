import React, {Fragment, useState} from 'react';
import SearchableDropdown from 'react-native-searchable-dropdown';
import {StyleSheet, Text, SafeAreaView, View} from 'react-native';


const beers = [
    {
        id: 1,
        name: 'Lager',
      },
      {
        id: 2,
        name: 'Stout',
      },
      {
        id: 3,
        name: 'Ale',
      },
      {
        id: 4,
        name: 'Porter',
      },
];

export default function Main() {
    const [selectedBeer, setSelectedBeer] = useState();


    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <Text style={styles.titleText}>
                Valitse olut
                </Text>
                <SearchableDropdown
                onTextChange={(text) => console.log(text)}
                selectedItems={selectedBeer}
                onItemSelect={(beer) => setSelectedBeer(beer)}
                containerStyle={{ padding: 5 }}
                textInputStyle={{
                  padding: 12,
                  borderWidth: 1,
                  borderColor: '#ccc',
                  backgroundColor: '#FAF7F6',
                }}
                itemStyle={{
                  padding: 10,
                  marginTop: 2,
                  backgroundColor: '#FAF9F8',
                  borderColor: '#bbb',
                  borderWidth: 1,
                }}
                itemTextStyle={{
                  color: '#222',
                }}
                itemsContainerStyle={{
                  maxHeight: '50%',
                }}
                items={beers}
                defaultIndex={2}
                placeholder="Etsi..."
                resetValue={false}
                underlineColorAndroid="transparent"
              />
            </View>
        </SafeAreaView>
        
    );
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
    },
    titleText: {
        padding: 8,
        fontSize: 16,
        textAlign: 'left',
        fontWeight: 'bold',
    },
    headingText: {
        padding: 8,
    },
});