import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './app/screens/Home';
import KyselyScreen from './app/screens/Kysely';
import TulosScreen from './app/screens/Tulos';
//import Pick from './screens/Pick';


const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} options={{headerShown:false}}/>
        <Stack.Screen name="Kysely" component={KyselyScreen} options={{headerShown:false}}/>
        <Stack.Screen name="Tulos" component={TulosScreen} options={{headerShown:false}}/>
      </Stack.Navigator>
  </NavigationContainer>
    /*<>
    <Pick />
    </>*/
  );
}


