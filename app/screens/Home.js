import * as React from 'react';
import { StyleSheet, Text, View, Image, Pressable, Dimensions } from 'react-native';
//colorsin importaaminen värejen suoraan kovakoodauksen tilalle mahdollistaa koko teeman värin vaihdon colors osiosta
import colors from '../../assets/colors/colors'


const HomeScreen = ({navigation}) => {
    return(
        <View style={styles.container}>
          {/* Header (logo)*/}
          <View style={styles.header}>
            <Image source = {require('../../assets/images/beervour.png')}
            style={styles.logo}
            resizeMode="stretch"
            />
          </View>
          {/* middle osioon on helppo laittaa sisältö ilman että se vaikuttaa sivun muuhun ulkoasuun. */}
          <View style={styles.middle}>
          <View style={styles.kuvansiirto}>
          <Image source = {require('../../assets/images/logo.png')}
          style={styles.logo}
          resizeMode="center"
          />
          </View>
            <View style={styles.tekstinsiirto}>
              <Text style={styles.fontti}>
                Tervetuloa päheimpään laskuriin
              </Text>
            </View>
          </View>
          {/* Footer (button)*/}
          <View style={styles.footer}>
            <View style={styles.button}>
            <View style={{flex: 1, justifyContent: 'flex-end', paddingTop: 40}}>
            <Pressable
              style={styles.nappi}
              onPress={() => navigation.navigate('Kysely')}
              >
              <Text style={styles.napinteksti}>Aloita</Text>
            </Pressable>
          </View>
            </View>
          </View>

        </View>
    )
}
const {height} = Dimensions.get("screen");
const {width} = Dimensions.get("screen");
const height_logo = height * 0.27
const width_logo = width * 0.95
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
    paddingTop: 40
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 38,
    paddingBottom: '7%',
  },
  middle: {
    flex: 3,
  },
  footer: {
    flex: 1,
    backgroundColor: colors.background,
  },
  logo: {
    width: width_logo,
    height: height_logo,
    
  },
  title: {
    color: colors.valkoinen,
    fontSize: 30,
    fontWeight: 'bold',
  },
  nappi: {
    height: 50,
    marginHorizontal: 20,
    backgroundColor: colors.valkoinen,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '8%',
    zIndex: 0.5
  },
  napinteksti: {
    fontSize: 24,
    color: colors.background,
    top: 0,
    left: 0,
    position: 'relative',
  },
  fontti: {
    paddingHorizontal: '5%',
    color: colors.valkoinen,
    fontStyle: 'italic',
    fontSize: 24,
  },
  kuvansiirto: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: '8%',
  },
  tekstinsiirto: {
    marginTop: 50,
    justifyContent: 'center',
    alignItems: 'center',
  }
  
});
export default HomeScreen;

