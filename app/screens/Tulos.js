import React from 'react';
import { StyleSheet, Text, View, Image, Pressable, Dimensions } from 'react-native';
//colorsin importaaminen värejen suoraan kovakoodauksen tilalle mahdollistaa koko teeman värin vaihdon colors osiosta
import colors from '../../assets/colors/colors'
import Icon from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';

Icon.loadFont();
Feather.loadFont

const TulosScreen = ({navigation,route}) => {


    return(
        <View style={styles.container}>
           {/* iconin koko huomioitu headerin paddingissa*/}
          <View style={styles.iconi}>
          <Pressable

              onPress={() => navigation.navigate('Kysely')}
              >
              <Icon name="arrow-back" size={24} color={colors.background} ></Icon>
          </Pressable>
            
          </View>
          {/* Header (logo)*/}
          <View style={styles.header}>
          
            <Image source = {require('../../assets/images/beervour2.png')}
            style={styles.logo}
            resizeMode="center"
            
            />
            
          </View>
          {/* middle osioon on helppo laittaa sisältö ilman että se vaikuttaa sivun muuhun ulkoasuun. */}
          <View style={styles.middle}>
            <View style={styles.wrapper}>
              <Text style={styles.titlesTitle}>Valitsemasi ruoka</Text>
            
              <View style={styles.searchWrapper}>
                  <View style={styles.search}>
                    <Text style={styles.searchText}>Valitsemasi ateria sisältäisi {Math.round(route.params.ruoat, 1)} kcal! Onneksi valitsit sen sijaan juoda maittavaa olutta!</Text>
                  </View>
              </View>
            </View>
            <View style={styles.wrapper}>
              <Text style={styles.titlesTitle}>Valitsemasi olut</Text>
              <View style={styles.searchWrapper}>
                <View style={styles.search}>
                  <Text style={styles.searchText}>Voit juoda {Math.round(route.params.beer, 1)} tuoppia valitsemaasi olutta ateriasi kaloreilla! Suosittelemme olueksi {route.params.beerrec}.</Text>
                </View>
              </View>
            </View>
          </View>
          {/* Footer (button)*/}
          <View style={styles.footer}>
            <View style={styles.button}>
            <View style={{flex: 1, justifyContent: 'flex-end', paddingTop: 40}}>
            <Pressable
              style={styles.nappi}
              onPress={() => navigation.navigate('Home')}
              >
              <Text style={styles.napinteksti}>Palaa alkuun</Text>
            </Pressable>
          </View>
            </View>
          </View>

        </View>
    )
}
const {height} = Dimensions.get("screen");
const {width} = Dimensions.get("screen");
const height_logo = height * 0.27
const width_logo = width * 0.9
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.secondary,
    paddingTop: 40
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: '7%',
  },
  middle: {
    backgroundColor: colors.valkoinen,
    flex: 3,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footer: {
    flex: 1,
    backgroundColor: colors.valkoinen,
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: 50
  },
  logo: {
    width: width_logo,
    height: height_logo
  },
  title: {
    color: colors.valkoinen,
    fontSize: 40,
    fontWeight: 'bold',
  },
  nappi: {
    height: 50,
    marginHorizontal: 20,
    backgroundColor: colors.background,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '8%',
  },
  napinteksti: {
    fontSize: 24,
    color: colors.valkoinen,
    fontFamily: 'Herculanum',
    fontWeight: 'bold'
  },
  iconi: {
    marginTop: 5,
    marginLeft: 5,
  },
  wrapper: {
    marginTop: 25,
    paddingHorizontal: 20,
  },
  titlesTitle: {
    paddingHorizontal: '5%',
    color: colors.background,
    //fontWeight: 'bold',
    fontSize: 32,
    //fontFamily: 'Curlz MT',
    fontFamily: 'Luminari',
  },
  searchWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginTop: 10,

  },
  search: {
    flex: 1,
    marginLeft: 10,
    borderBottomColor: '#9F9F9F',
    borderWidth: 2,
    borderRadius: 10,
    borderColor: '#9F9F9F',

  },
  searchText: {
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 5,
    marginTop: 5,
    color: 'black',
    marginLeft: 5,
    fontFamily: 'Batang'
  }

});
export default TulosScreen;

