import React,{useState,useEffect} from 'react';
import { StyleSheet, Text, View, Image, Pressable, Dimensions, ImageBackground } from 'react-native';
//colorsin importaaminen värejen suoraan kovakoodauksen tilalle mahdollistaa koko teeman värin vaihdon colors osiosta
import colors from '../../assets/colors/colors'
import { Picker } from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';



Icon.loadFont();
Feather.loadFont

const KyselyScreen = ({navigation}) => {

    const [selectedBeer, setSelectedBeer] = useState(0);
    const [beerRec, setBeerRec] = useState('');
    const [selectedFood, setSelectedFood] = useState(0)
    const [numberBeers, setNumberBeers] = useState(0);

    
    // Lista oluista
    const beers = Array();
    beers.push({label: '', value: ''});
    beers.push({label: 'IPA', value: 1});
    beers.push({label: 'Lager', value: 2});
    beers.push({label: 'Pils', value: 3});
    beers.push({label: 'Stout/Portteri', value: 4});
    beers.push({label: 'Vehnäolut', value: 5});
    // Lista ruoista 
    const ruoat = Array();
    ruoat.push({label: '', value: ''});
    ruoat.push({label: 'Hampurilainen, kerroshampurilainen, hesburger', value: 610});
    ruoat.push({label: 'Hampurilainen, kerroshampurilainen, big mac, mcdonalds', value: 534});
    ruoat.push({label: 'Kaurapuuro, kevytmaito, suolaa', value: 230});
    ruoat.push({label: 'Lihapiirakka', value: 341});
    ruoat.push({label: 'Makkarakeitto', value: 216});
    ruoat.push({label: 'Makaronilaatikko, liha-makaronilaatikko, atria, valmisateria', value: 434});
    ruoat.push({label: 'Omenapiirakka, muropohja', value: 241});
    ruoat.push({label: 'Pannukakku, kevytmaito', value: 259});
    ruoat.push({label: 'Pizza, jauhelihapizza, atria', value: 393});
    ruoat.push({label: 'Pulla, dallaspulla, fazer', value: 210});
    ruoat.push({label: 'Salaatti, jäävuorisalaatti, tomaatti, kurkku, vihersekasalaatti', value: 20});
    ruoat.push({label: 'Sienikeitto', value: 295});
    ruoat.push({label: 'Smoothie, mango-ananas-kookosmaito', value: 224});
    ruoat.push({label: 'Täytekakku, kinuskikakku, frödinge, pakaste', value: 134});

    // Suomen parhaat oluet

    const ipa = ['Juiciness, CoolHead Brew', 'New England IPA, Stadin Panimo Oy', 'Hopz N the Hood, Olarin Panimo Oy'];
    const lager = ['Rock - An allday,everyday Lager, RPS Brewing Oy', 'Märichello, Olarin Panimo Oy', 'Karhu 5,3, Oy Sinebrychoff Ab'];
    const pils = ['Kukko Pils, Laitilan Wirvoitusjuomatehdas Oy', 'Bryggeri Pils, Bryggeri Helsinki', 'Galaxy Pils, Stadin Panimo Oy'];
    const stout = ['Ikiiurso Bourbon Barrel Aged, Panimoyhtiö Hiisi Oy', 'Pyöveli, Panimoyhtiö TuJu Oy', 'Laitilan Imperiaali, Laitilan Wirvoitusjuomatehdas Oy'];
    const vehna = ['Valo vehnäolut, Panimo Honkavuori Oy', 'Dandelion Dreams Witbier, RPS Brewing Oy', 'Heili, Panimo Honkavuori Oy'];

    /*
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''});
    ruoat.push({label: '', value: ''}); 
    */
    useEffect(() => {
      numberofbeers();
      recommend();
      }
    , [selectedFood,selectedBeer]);

    // Lasketaan kuinka monta olutta voidaan sisällyttää valittuun annokseen
    function numberofbeers() {
      let kcals = 0;
      kcals = selectedFood / 213;
      setNumberBeers(kcals);
    };
    // Viedään olutsuositus valitun laadun mukaan
    function recommend () {
      let i=Math.floor(Math.random() * 3);
      if (selectedBeer === "1"){
        setBeerRec(ipa[i]);
      }else if(selectedBeer === "2"){
        setBeerRec(lager[i]);
      }else if(selectedBeer === "3" ){
        setBeerRec(pils[i]);
      }else if(selectedBeer === "4"){
        setBeerRec(stout[i]);
      }else{
        setBeerRec(vehna[i]);
      }
    };

    
    return(
        <View style={styles.container}>
           {/* iconin koko huomioitu headerin paddingissa*/}
          <View style={styles.iconi}>
          <Pressable

              onPress={() => navigation.navigate('Home')}
              >
              <Icon name="arrow-back" size={24} color={colors.background} ></Icon>
          </Pressable>
            
          </View>
          {/* Header (logo)*/}
          <View style={styles.header}>
          
            <Image source = {require('../../assets/images/beervour2.png')}
            style={styles.logo}
            resizeMode="stretch"
            
            />
            
          </View>
          {/* middle osioon on helppo laittaa sisältö ilman että se vaikuttaa sivun muuhun ulkoasuun. */}
          <View style={styles.middle}>
            <View style={styles.wrapper}>
              <Text style={styles.titlesTitle}>Valitse Ruoka</Text>
            
              <View style={styles.searchWrapper}>
                <Feather name="search" size={20} color={colors.background}></Feather>
                  <View>
                    <Picker
                    style={{height: 30, width: 320, marginLeft: 10}}
                      onValueChange={(itemValue) => setSelectedFood(itemValue)}
                      selectedValue={selectedFood}>
                      {ruoat.map((ruoat,index) => (
                        <Picker.Item key={index} label={ruoat.label} value={ruoat.value}/>
                        ))
                      }
                    </Picker>
                  </View>
              </View>
            </View>
          <View style={styles.wrapper}>
            <Text style={styles.titlesTitle}>Valitse Juoma</Text>
            
            <View style={styles.searchWrapper}>
              <Feather name="search" size={20} color={colors.background}></Feather>
                <View>
                  <Picker
                    style={{height: 30, width: 320, marginLeft: 10}}
                    onValueChange={(itemValue) => setSelectedBeer(itemValue)}
                    selectedValue={selectedBeer}>
                    {beers.map((beers,index) => (
                      <Picker.Item key={index} label={beers.label} value={beers.value}/>
                      ))
                      }
                  </Picker>
                </View>
              </View>
           </View>
          </View>
          {/* Footer (button)*/}
          <View style={styles.footer}>
            <View style={styles.button}>
            <View style={{flex: 1, justifyContent: 'flex-end', paddingTop: 40}}>
            <Pressable
              style={styles.nappi}
              onPress={() =>{numberofbeers(); navigation.navigate('Tulos',
              {beer:numberBeers,
               ruoat:selectedFood,
               beerrec:beerRec,
              })
            }}
              >
              <Text style={styles.napinteksti}>Tulos</Text>
            </Pressable>
          </View>
            </View>
          </View>

        </View>
    )
}
const {height} = Dimensions.get("screen");
const {width} = Dimensions.get("screen");
const height_logo = height * 0.27
const width_logo = width * 0.9
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.secondary,
    paddingTop: 40
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: '7%',
  },
  middle: {
    backgroundColor: colors.valkoinen,
    flex: 3,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  footer: {
    flex: 1,
    backgroundColor: colors.valkoinen,
  },
  logo: {
    width: width_logo,
    height: height_logo
  },
  title: {
    color: colors.valkoinen,
    fontSize: 40,
    fontWeight: 'bold',
  },
  nappi: {
    height: 50,
    marginHorizontal: 20,
    backgroundColor: colors.background,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '8%',
  },
  napinteksti: {
    fontSize: 24,
    color: colors.valkoinen
  },
  iconi: {
    marginTop: 5,
    marginLeft: 5,
  },
  wrapper: {
    marginTop: 40,
    paddingHorizontal: 20,
  },
  titlesTitle: {
    paddingHorizontal: '5%',
    color: colors.background,
    fontStyle: 'italic',
    fontSize: 32,
    fontFamily: 'Comfortaa',
  },
  searchWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginTop: 20,

  },
  search: {
    flex: 1,
    marginLeft: 10,
    borderBottomColor: '#9F9F9F',
    borderWidth: 2,
    borderRadius: 15,
    borderColor: '#9F9F9F',

  },
  searchText: {
    fontStyle: 'normal',
    fontSize: 14,
    marginBottom: 5,
    marginTop: 5,
    color: '#9F9F9F',
    marginLeft: 5,
  }

});
export default KyselyScreen;

